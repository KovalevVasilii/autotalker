package com.example.witcher.talker

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.speech.RecognizerIntent
import android.content.Intent
import java.util.*
import android.widget.*
import java.lang.Exception
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private var contacts = ArrayList<Contact>()
    private var contactsName = ArrayList<String>()
    private var possibleContacts = ArrayList<Contact>()
    private var currMsg: String = String()
    private var currUserName: String = String()
    private var currUserNumber: String? = null
    private var availableContacts: Int? = null
    private var mSpinner: Spinner? = null
    private var adapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        availableContacts = intent.getIntExtra("CONTACTS",0)
        contacts = Contacts.contacts

        mSpinner = findViewById(R.id.cities) as Spinner

    }

    fun getSpeechInput(view: View) {

        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())

        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, 10)
        } else {
            Toast.makeText(this, getString(R.string.device_does_not_support), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && data != null) {
            val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            defineMessage(result[0])
            contactsName.clear()
            adapter?.notifyDataSetChanged();
            currUserNumber = null

            if(availableContacts != 0) {
                findContact()
            }else{
                currUserNumber = currUserName
            }
            //
            if(possibleContacts.size != 1) {
                for (conn in possibleContacts) {
                    contactsName.add(conn?._name ?: "null")
                }
                adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, contactsName)
                adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                mSpinner?.adapter = adapter
                val itemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {


                        currUserNumber = possibleContacts[position]._phone
                        if (currUserNumber == null) {
                            currMsg = ""
                            Toast.makeText(this@MainActivity, getString(R.string.receiver_not_found), Toast.LENGTH_SHORT).show()
                            return
                        }
                        sendMessage()

                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                        sendMessage()
                    }
                }
                mSpinner?.onItemSelectedListener = itemSelectedListener
            }else {

                if (currUserNumber == null) {
                    currMsg = ""
                    Toast.makeText(this, getString(R.string.receiver_not_found), Toast.LENGTH_SHORT).show()
                    return
                }
                sendMessage()
            }




        }
        else{
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendMessage(){
        phoneNumberTreatment()

        val sendIntent = Intent()
        sendIntent.putExtra("jid", "$currUserNumber@s.whatsapp.net")
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, currMsg)
        sendIntent.type = "text/plain"
        sendIntent.setPackage("com.whatsapp")

        if (sendIntent.resolveActivity(packageManager) == null) {
            Toast.makeText(this, getString(R.string.whatsapp_not_installed), Toast.LENGTH_SHORT).show()
            return
        }

        startActivity(sendIntent)
    }
    private fun defineMessage(string : String){
        try {
            var end = string.indexOf(getString(R.string.message))
            var msg = string.substring(end + getString(R.string.message).length)
            var user = string.substring(getString(R.string.send).length, end)
            currMsg = msg.trim()
            currUserName = user.trim()
        }
        catch (e: Exception){
            Toast.makeText(this, getString(R.string.wrong_format_of_msg), Toast.LENGTH_SHORT).show()
        }

    }

    private fun findContact(){
        possibleContacts.clear()
        val needName = currUserName?.toLowerCase()

        for(Conn in contacts){
            val connName = Conn._name?.toLowerCase()
            if(connName == null){
                continue
            }
            if (connName == needName){
                currUserNumber = Conn._phone
                possibleContacts.add(Conn)
                //Toast.makeText(this, ""+connName+"/"+needName, Toast.LENGTH_SHORT).show()
                continue
            }
            if(connName?.contains(needName)){
                possibleContacts.add(Conn)
                //Toast.makeText(this, ""+connName+"/"+needName, Toast.LENGTH_SHORT).show()
            }
        }
        if(possibleContacts.size == 1){
            currUserNumber = possibleContacts.get(0)._phone
        }

    }

    private fun phoneNumberTreatment(){
        var toNumber = currUserNumber
        toNumber = toNumber?.replace("+", "")?.replace(" ", "")
        if(toNumber?.get(0) == '8'){
            toNumber = "7" + toNumber.substring(1)
        }
        currUserNumber = toNumber
    }


}
