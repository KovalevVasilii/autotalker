package com.example.witcher.talker

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.content.Intent
import android.os.Handler
import android.os.Message
import android.widget.Toast


class SplashActivity : AppCompatActivity() {
    private var contacts = ArrayList<Contact>()
    private var PERMISSION_REQUEST_READ_CONTACTS = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)

        requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST_READ_CONTACTS)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
            val runnable = Runnable {
                val msg = handler.obtainMessage()
                getContacts()
                handler.sendMessage(msg)
            }
            val thread = Thread(runnable)
            thread.start()
        }
        else{
            Toast.makeText(this, getString(R.string.need_full_number), Toast.LENGTH_SHORT).show()

            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.putExtra("CONTACTS",0)
            startActivity(intent)
            finish()
        }

    }

    var handler: Handler = object : Handler() {
        override fun  handleMessage(msg: Message) {
            val intent = Intent(applicationContext, MainActivity::class.java)
            Contacts.contacts = contacts
            intent.putExtra("CONTACTS",1)
            startActivity(intent)
            finish()
        }
    }
    private fun getContacts() {

        var phoneNumber: String = String()

        val CONTENT_URI = ContactsContract.Contacts.CONTENT_URI
        val _ID = ContactsContract.Contacts._ID
        val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME
        val HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER

        val PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID
        val NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER

        val contentResolver = contentResolver
        val cursor = contentResolver.query(CONTENT_URI, null, null, null, null)
        if (cursor.count > 0) {

            while (cursor.moveToNext()) {
                var contact = Contact()
                val contact_id = cursor.getString(cursor.getColumnIndex(_ID))
                val name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME))
                val hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)))

                if (hasPhoneNumber > 0) {
                    if(name != null){
                        contact._name = name.trim()
                    }else{
                        continue
                    }

                    val phoneCursor = contentResolver.query(
                        PhoneCONTENT_URI, null,
                        Phone_CONTACT_ID + " = ?", arrayOf<String>(contact_id), null
                    )

                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER))
                        contact._phone = phoneNumber.trim()
                    }
                }
                contacts.add(contact)

            }
        }
    }
}